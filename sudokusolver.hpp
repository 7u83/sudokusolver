
class SudokuSolver
{
public:
	int maxresults;
	int results;

	SudokuSolver()
	{
		maxresults=1;
		results=0;
	}
	

	virtual void saveresult(const char *s)
	{

	}

	
	//
	// check if the the char v would lead to a conflict 
	// if placed in sudoku s at position p
	// retrun true if no conflict is detected
	// otherwise false
	// 
	bool check(const char * s,int p,char v)
	{

		int x = p%9;
		int y = p/9;


		int qx=(int)(x/3)*3;
		int qy=(int)(y/3)*3;

		for(int r=0; r<3; r++)
		{
			int cp = (qy+r)*9+qx;
			for( int c=0; c<3 ; c++)
			{
				if ( c+cp != p && s[c+cp]==v)
				{
					return false;
				}
			}
		}

		for ( int i=0; i<9; i++)
		{
			if (y*9+i !=p && s[y*9+i]==v)
			{
				return false;
			}
			
			if (x+i*9 != p && s[x+i*9]==v)
			{
				return false;
			}
		}
			
		return true;	
	}

	virtual const char * getcharlist()
	{
		return "123456789";
	}

	// 
	// solve the sudoku sp
	//
	bool solve(const char * sp)
	{

		char s[81];
		memcpy(s,sp,81);

		int bestn=10;
		int bestp=0;
		char bestclist[9];
		
		for (int p=0;p<81;p++)
		{
			if (s[p]=='0')
			{
				int n=0;
				char clist[9];
				const char *cl = getcharlist();
				
				for (int cp=0;cp<9;cp++) 
				{
					char c=cl[cp];
					if (check(s,p,c))
					{
						clist[n]=c;
						n++;
					}
				}
				if (n<bestn)
				{
					memcpy(bestclist,clist,n);
					bestn=n;
					bestp=p;
				}
				if (n==0)
				{
					return false;
				}

			}
		}
		

		if ( bestn==10)
		{
			results++;
			saveresult(s);
			return true;
		}


		bool rc;
		for (int i=0;i<bestn;i++)
		{
			s[bestp]=bestclist[i];
			rc=solve(s);
			if ( rc && results>maxresults-1)
				return true;
		}
		return rc;
			
	}
};


