/**
 * Sudokusolver
 * erstellt von Tube (www.surfpoeten.de)
 */


#include <string.h>
#include <stdlib.h>
#include <stdio.h>


#include <unistd.h>
#include <pthread.h>
#include <signal.h>


#include "sudokusolver.hpp"


//
// a class witch solves sudokus and saves the 
// results to a file
//
class SudokuSolverFile:public SudokuSolver
{
public:
	FILE *outfile;
	void saveresult(const char *s)
	{
		fwrite(s,1,81,outfile);
		fprintf(outfile,"\n");
		return;
	}

	virtual const char * getcharlist()
	{
		return "987654321";
	}
};



pid_t pid;

//
// a thread wich kills the program after 60 seconds
// 
void * watchdog( void *ptr)
{
	for (int i=0;i<60;i++)
	{
		sleep(1);
	}
	kill(pid,SIGKILL);
	return 0;
}




int main(int argc, char *argv[])
{
//	printf("This is the Sudoku Solver\n");
/*	for (int i=0;i<argc;i++)
		printf("ARG%i: %s\n",i,argv[i]);
*/	
	if ( argc != 4 )
	{
		printf("Bad number of args\n");
		return 1;
	}


	// create the watchdog thread	
	pid = getpid();
	pthread_t wdthread;
	pthread_create(&wdthread,NULL,watchdog,NULL);
//	printf("PID: %i\n",pid);
//	printf("TID: %i\n",wdthread);


	// copy and check the sudoko from args
	char s[81];
	memset(s,'0',81);
	for( int i=0; i<81 && argv[1][i]!='\0'; i++)
	{
		s[i]=argv[1][i];
		if (s[i]<'0' || s[i]>'9')
		{
			printf("Bad sudoku\n");
			return 1;
		}
	}

	// open the output file
	FILE * outfile=fopen(argv[2],"wt");
	if ( outfile==NULL )
	{
		printf("Can't open outfile");
		return 1;
	}

	SudokuSolverFile ss = SudokuSolverFile();
	ss.outfile=outfile;
	ss.maxresults = atoi(argv[3]);
	ss.solve(s);
	fclose(outfile);
	return 0;
}
